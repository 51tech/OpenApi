package org.pp.openapi.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 关联多表数据
 */
@Data
public class RelData {

    /**
     * 忽略事件
     */
    private Boolean ignore;

    /**
     * 数据表集合
     */
    private List<String> tags = new ArrayList<>();

    /**
     * 数据集合
     */
    private List<TableData> tables = new ArrayList<>();

    /**
     * 加入一个数据表
     * @param table
     */
    public void addTable(TableData table){
        tables.add(table);
    }
}
