package org.pp.openapi.consts;

/**
 * 内置SQL类型
 */
public enum SqlType {
    TABLE("table","列表"),
    COUNT("count","计算"),
    CHART("chart","报表"),
    INNER("inner","子查询"),
    BATCH("batch","数据批处理"),;
    private String code;
    private String name;

    SqlType(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
