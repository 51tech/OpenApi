package org.pp.openapi.vo;

import org.pp.openapi.consts.QueryType;
import lombok.Data;
import org.pp.openapi.utils.StrUtil;

import java.util.HashMap;
import java.util.Map;

@Data
public class RelQuery extends TableQuery{
    /**
     * 请业务表
     */
    private TableQuery mainTable;

    /**
     * 子业务表
     */
    private Map<String,TableQuery> subs= new HashMap<>();

    /**
     * 数据联表
     */
    private Map<String,TableQuery> joins= new HashMap<>();

    /**
     * 当前的查询类型 @com.ruoyi.dev.sql.QueryType
     * @return int
     */
    public int getType(){
        if(!subs.isEmpty()){
            return QueryType.SINGLE_REL;
        }else if(joins.isEmpty()){
            if(pager != null){
                return QueryType.SINGLE_PAGE;
            }else{
                return QueryType.SINGLE;
            }
        }else{
            if(pager != null){
                return QueryType.JOIN_PAGE;
            }else{
                return QueryType.JOIN_SINGLE;
            }
        }
    }

    /**
     * 判断是否为联表查询
     * @return
     */
    public boolean isJoin(){
        return !joins.isEmpty();
    }

    /**
     * 添加一个表
     * @param table
     */
    public void addTable(TableQuery table){
        if(StrUtil.isEmpty(table.getJoin())){
            if(mainTable == null){
                mainTable = table;
                setTable(table.getTable());
            }else{
                subs.put(table.getTable(), table);
            }
        }else{
            joins.put(table.getTable(), table);
        }
    }

    /**
     * 生成查询SQL
     * @return
     */
    @Override
    public String getSql(String dataScope){
        StringBuilder fields = new StringBuilder();
        StringBuilder wheres = new StringBuilder();
        StringBuilder sql = new StringBuilder("");
        String having = "";
        String group = "";
        //主表
        sql.append("\nFROM ").append(mainTable.getName(true)).append(" ").append("\n");
        //主表数据权限
        if(dataScope != null){
            wheres.append("AND ").append(mainTable.getAlias()).append(".").append(dataScope).append("\n");
        }
        //主表查询条件
        if(mainTable.getData() != null && !mainTable.getData().isEmpty()){
            wheres.append("AND ").append(parseWhere(mainTable)).append("\n");
        }
        //主表字段
        if(StrUtil.isEmpty(mainTable.getFields())){
            fields.append(",").append(mainTable.getAlias()).append(".*");
        }else{
            fields.append(mainTable.getField());
        }
        //主表having
        if(StrUtil.isNotEmpty(mainTable.getHaving())){
            having += "AND "+ mainTable.getName() + mainTable.getHaving() + " ";
        }
        //主表group
        if(StrUtil.isNotEmpty(mainTable.getGroup())){
            group += ","+ mainTable.getName() + mainTable.getGroup();
        }
        //从表
        for(TableQuery table :joins.values()){
            if(StrUtil.isEmpty(table.getJoin())){
                continue;
            }
            sql.append(parseJoin(table));

            fields.append(table.getField());
            if(table.getData() != null && !table.getData().isEmpty()){
                wheres.append("AND ").append(parseWhere(table)).append("\n");
            }
            if(StrUtil.isNotEmpty(table.getHaving())){
               having += "AND " + table.getName() + table.getHaving() + " ";
            }
            if(StrUtil.isNotEmpty(table.getGroup())){
                group += "," + table.getName() + table.getGroup();
            }
        }
        //去除开头拼接的,
        sql.insert(0, fields.toString().substring(1)).insert(0, "SELECT \n");
        //查询条件
        if(wheres.length() > 0){
            sql.append(" WHERE ").append(wheres.toString().substring(4));
        }
        if(having.length() > 0){
            sql.append(" HAVING ").append(having.substring(4)).append("\n");
        }
        if(group.length() > 0){
            sql.append(" GROUP BY ").append(group.substring(1)).append("\n");
        }

        return sql.toString();
    }

    /**
     * 配置联表
     * @param table
     */
    public String parseJoin(TableQuery table){
        StringBuilder sb = new StringBuilder(" JOIN ");
        sb.append(table.getName(true)).append(" ON ");
        String joinStr = table.getJoin().trim();
        if("LEFT".equalsIgnoreCase(joinStr.substring(0,4))){
            sb.insert(0,"LEFT ").append(joinStr.substring(4));
        }else if("RIGHT".equalsIgnoreCase(joinStr.substring(0,5))){
            sb.insert(0,"RIGHT ").append(joinStr.substring(5));
        }else{
            sb.append(joinStr);
        }
        return sb.toString();
    }

    public static void main(String[] args){
        String str = "USER/id/";
        String[] sa = str.split("\\/");
        System.out.println(sa.length);
        for(String s:sa){
            System.out.println(s);
        }
    }
}
