package org.pp.openapi.common;

import lombok.Data;

/**
 * 用户模型
 */
@Data
public class ApiUser {
    /** 用户主键 **/
    private String userId;
    /** 用户账号 **/
    private String account;
    /** 用户姓名 **/
    private String username;
    /** 用户角色数组 **/
    private String[] roles;
    /** 用户所在组织 **/
    private String deptId;
}
