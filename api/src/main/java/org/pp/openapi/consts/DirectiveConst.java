package org.pp.openapi.consts;

/**
 * 指令集
 */
public class DirectiveConst {

    /** 数据标签 **/
    public static String TAGS = "@tags";

    /** 分页指令 **/
    public static String PAGER = "@pager";

    /** 字段指令 **/
    public static String COLUMN = "@column";

    /** 过滤指令 **/
    public static String HAVING = "@having";

    /** 分组指令 **/
    public static String GROUP = "@group";

    /** 模式指令 **/
    public static String SCHEMA = "@schema";

    /** 表别名指令 **/
    public static String ALIAS = "@alias";

    /** 表名称指令 **/
    public static String TABLE = "@table";

    /** 条件组合指令 **/
    public static String COMBINE = "@combine";

    /** 连表指令 **/
    public static String JOIN = "@join";

    /** 查询排序指令 **/
    public static String ORDER = "order";

    /** 分页行数指令 **/
    public static String LIMIT = "limit";

    /** 当前页码指令 **/
    public static String PAGE = "page";

    /** 条件表达式指令 **/
    public static String EXP = "@exp";

    /** 导入导出配置指令 **/
    public static String EXCEL = "@excel";

    /** 数据字典指令 **/
    public static String DICT = "@dict";

    /** 数据源指令 **/
    public static String DS = "@ds";

    /** 忽略事件 **/
    public static String IGNORE = "@ignore";
}
