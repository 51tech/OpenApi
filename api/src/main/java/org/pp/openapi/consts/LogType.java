package org.pp.openapi.consts;

/**
 * 日志记录类型
 */
public enum LogType {
    INSERT("insert","新增数据"),
    UPDATE("update","更新数据"),
    DELETE("delete","删除数据"),
    IMPORT("import","导入数据"),
    LOGIN("login","登录系统"),
    LOGOUT("logout","退出系统"),
    EVENT("event","触发事件"),
    ERROR("error","数据错误"),;
    private String code;
    private String name;

    LogType(String code, String name){
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
