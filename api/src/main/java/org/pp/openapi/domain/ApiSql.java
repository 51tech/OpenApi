package org.pp.openapi.domain;

import lombok.Data;

import java.util.Map;

/**
 * 内置SQL
 */
@Data
public class ApiSql {
    /** 主键ID **/
    private Long id;

    /** 分组 **/
    private String group;

    /** 类型 **/
    private String type;

    /** 代码 **/
    private String code;
    /** 名称 **/
    private String name;

    /** JS模拟脚本 **/
    private String content;

    /** 说明 **/
    private String remark;

    /** 是否内置模型 **/
    private Boolean isDefault;

    /**
     * 判断当前脚本是否为JS动态函数
     * @return
     */
    public Boolean isFun(){
        return content.contains("function ") && content.contains(" exec");
    }

    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiSql fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("code")){
            return null;
        }
        ApiSql sql = new ApiSql();
        sql.setId(Long.parseLong(data.get("id").toString()));
        sql.setGroup(data.getOrDefault("group", "默认").toString());
        sql.setType(data.get("type").toString());
        sql.setCode(data.get("code").toString());
        sql.setName(data.get("name").toString());
        sql.setContent(data.get("content").toString());
        sql.setRemark(data.getOrDefault("remark", "").toString());
        sql.setIsDefault("Y".equalsIgnoreCase(data.getOrDefault("is_default", "N").toString()));
        return sql;
    }
}
