package org.pp.openapi.service;

import org.pp.openapi.common.ApiResult;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface IOpenDataService {
    /**
     * 执行接口
     *
     * @param request 请求对象
     * @param body 请求内容
     * @return 脚本
     */
    public ApiResult run(HttpServletRequest request, String body);

    /**
     * 查询数据
     * @param paramsStr
     * @return
     */
    public Map doGet(String paramsStr);

    /**
     * 删除数据
     * @param paramsStr
     * @return
     */
    public int doDelete(String paramsStr);

    /**
     * 新增数据，如果模型配置了唯一字段，就会唯一字段做查询判断是否有旧数据，如果有旧数据，就认为是重复，报错
     * @param paramsStr
     * @return
     */
    public Map doPost(String paramsStr);

    /**
     * 更新数据，需要有主键数据，没有就报错
     * @param paramsStr
     * @return
     */
    public int doPut(String paramsStr);

    /**
     * 更新或新增
     * 如果带有主键数据，会查询是否有旧数据：如果有旧数据会自动更新，没有旧数据会新增
     * 如果没有主键数据，但是模型配置了唯一字段，会按唯一字段做查询判断是否有旧数据：有旧数据就更新，没有旧数据就新增
     * @param paramsStr
     * @return
     */
    public int doPatch(String paramsStr);

    /**
     * 查询数据源，支持类型是统计报表，列表查询和计算，不支持子查询和批处理
     * @param dsCode
     * @param params
     * @return
     */
    public Object doDs(String dsCode, Map params);
}
