package org.pp.openapi.common;

import com.ruoyi.common.exception.ServiceException;

/**
 * 异常类
 */
public class ApiException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     *
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public ApiException()
    {
    }

    public ApiException(String message)
    {
        this.message = message;
    }

    public ApiException(String message, Integer code)
    {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public Integer getCode()
    {
        return code;
    }

    public ApiException setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public ApiException setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
        return this;
    }
}
