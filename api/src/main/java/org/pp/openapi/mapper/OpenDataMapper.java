package org.pp.openapi.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.*;
import org.pp.openapi.sql.OpenDataSqlProvider;

/**
 * 模型Mapper接口
 * 
 * @author yepanpan
 * @date 2020-12-29
 */
public interface OpenDataMapper
{

    /**
     * 查询一条数据
     *
     * @param apiMap SQL
     * @return 模型集合
     */
    @SelectProvider(type = OpenDataSqlProvider.class, method = "find")
    public Map findByMap(Map apiMap);

    /**
     * 分页查询数据
     *
     * @param apiMap SQL
     * @return 模型集合
     */
    @SelectProvider(type = OpenDataSqlProvider.class, method = "select")
    public List<Map> selectByMap(Map apiMap);

    /**
     * 删除数据
     *
     * @param apiMap SQL
     * @return 模型集合
     */
    @DeleteProvider(type = OpenDataSqlProvider.class, method = "delete")
    public int deleteByMap(Map apiMap);

    /**
     * 新增数据
     *
     * @param apiMap SQL
     * @return 模型集合
     */
    @Options(useGeneratedKeys = true, keyColumn = "id", keyProperty = "id")
    @InsertProvider(type = OpenDataSqlProvider.class, method = "insert")
    public int insertMap(Map apiMap);

    /**
     * 更新数据
     *
     * @param apiMap SQL
     * @return 模型集合
     */
    @UpdateProvider(type = OpenDataSqlProvider.class, method = "update")
    public int updateMap(Map apiMap);

    /**
     * 执行一条新增、更新或删除
     *
     * @param sql SQL
     * @return 模型集合
     */
    @UpdateProvider(type = OpenDataSqlProvider.class, method = "exec")
    public Integer exec(String sql);

    /**
     * 执行一次查询
     *
     * @param sql SQL
     * @return 模型集合
     */
    @SelectProvider(type = OpenDataSqlProvider.class, method = "exec")
    public List<Map> query(String sql);
}
