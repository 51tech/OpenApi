package com.ruoyi.web.api;

import org.pp.openapi.service.IApiLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 启动加载
 */
@Component
public class ApiRunner implements ApplicationRunner {
    @Autowired
    private IApiLoaderService apiLoaderService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        apiLoaderService.loadModel();
        apiLoaderService.loadEvent();
        apiLoaderService.loadSql();
    }
}
