package org.pp.openapi.domain;

import lombok.Data;

/**
 * 模型字段
 */
@Data
public class ApiField {
    /** 主键ID **/
    private Long id;

    /** 所属模型 **/
    private Long modelId;

    /** 字段类型 **/
    private Integer type;

    /** 字段代码 **/
    private String code;

    /** 字段名称 **/
    private String name;

    /** 长度 **/
    private Integer length;

    /** 小数位 **/
    private Integer scale;

    /** 可为空 **/
    private Boolean nullable;

    /** 详细说明 **/
    private String remark;
}
