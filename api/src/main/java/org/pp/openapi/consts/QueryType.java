package org.pp.openapi.consts;

/**
 * 查询模式
 */
public class QueryType {

    //单表单条查询
    public static final int SINGLE = 1;

    //单表分页查询
    public static final int SINGLE_PAGE = 12;

    //多表单条查询
    public static final int SINGLE_REL = 20;

    //联表单条查询
    public static final int JOIN_SINGLE = 21;

    //联表分页查询
    public static final int JOIN_PAGE = 22;

}
