package org.pp.openapi.vo;

/**
 * Excel字段
 */

import lombok.Data;

@Data
public class ExcelField {
    /** 序号 **/
    private Integer index;
    /** 类型 val dict ds **/
    private String type;

    /** 数据表 **/
    private String dataTable;

    /** 数据字段 **/
    private String dataField;

    /** 数据标题 **/
    private String dataLabel;
}
