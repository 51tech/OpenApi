package org.pp.openapi.common;

import org.pp.openapi.service.IApiLoaderService;
import org.pp.openapi.service.IOpenApiService;
import org.pp.openapi.service.IOpenDataService;
import org.pp.openapi.service.IOpenExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 开放数据接口
 */
public class OpenDataController
{
    @Autowired
    private IOpenApiService openApiService;
    @Autowired
    private IOpenDataService openDataService;
    @Autowired
    private IApiLoaderService apiLoaderService;

    @Autowired
    private IOpenExcelService openExcelService;

    /**
     * 标准Restful
     */
    @RequestMapping("/")
    public ApiResult curd(HttpServletRequest request, @RequestBody(required = false) String body)
    {
        return openDataService.run(request, body);
    }

    /**
     * 标准Restful
     */
    @GetMapping("/{query}")
    public ApiResult curd(@PathVariable("query") String query)
    {
        return ApiResult.success(openDataService.doGet(query));
    }

    /**
     * 查询
     */
    @RequestMapping("/get")
    public ApiResult get(@RequestBody String query)
    {
        return ApiResult.success(openDataService.doGet(query));
    }

    /**
     * 新增
     */
    @RequestMapping("/post")
    public ApiResult post(@RequestBody String body)
    {
        return ApiResult.success(openDataService.doPost(body));
    }

    /**
     * 更新
     */
    @RequestMapping("/put")
    public ApiResult put(@RequestBody String body)
    {
        return ApiResult.success(openDataService.doPut(body));
    }

    /**
     * 更新或新增
     */
    @RequestMapping("/patch")
    public ApiResult patch(@RequestBody String body)
    {
        return ApiResult.success(openDataService.doPatch(body));
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public ApiResult delete(@RequestBody String body)
    {
        return ApiResult.success(openDataService.doDelete(body));
    }

    /**
     * 查询数据源
     */
    @GetMapping("/ds/{code}")
    public ApiResult ds(@PathVariable("code") String code, @RequestParam Map data)
    {
        return ApiResult.success(openDataService.doDs(code, data));
    }

    /**
     * 重载缓存
     */
    @GetMapping("/reload")
    public ApiResult reload(@RequestParam("type") String type, @RequestParam("ids") Long ids[])
    {
        if(!openApiService.isAdmin()){
            return ApiResult.error("您没有权限");
        }
        return ApiResult.success(apiLoaderService.reload(type, ids));
    }

    /**
     * 查询数据源
     */
    @PostMapping("/export")
    public void export(@RequestBody String body, HttpServletResponse response)
    {
        try {
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setCharacterEncoding("utf-8");
            String fileName = "导出.xlsx";
            String downloadFileName = new String(fileName.getBytes("UTF-8"), "UTF-8");
            response.setHeader("attachment", downloadFileName);
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM.getType());
            openExcelService.exports(body, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
