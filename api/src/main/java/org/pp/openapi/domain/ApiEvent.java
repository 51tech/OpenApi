package org.pp.openapi.domain;

import lombok.Data;

import java.util.Map;

/**
 * 自定义模型事件
 */
@Data
public class ApiEvent {
    /** 主键ID **/
    private Long id;

    /** 模型 **/
    private Long modelId;

    /** 事件 **/
    private String action;

    /** JS模拟脚本 **/
    private String content;

    /** 状态 **/
    private String status;

    /** 是否内置模型 **/
    private Boolean isDefault;

    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiEvent fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("model_id")){
            return null;
        }
        ApiEvent event = new ApiEvent();
        event.setId(Long.parseLong(data.get("id").toString()));
        event.setModelId(Long.parseLong(data.get("model_id").toString()));
        event.setAction(data.get("action").toString());
        event.setContent(data.get("content").toString());
        event.setStatus(data.getOrDefault("status", "1").toString());
        event.setIsDefault("Y".equalsIgnoreCase(data.getOrDefault("is_default", "N").toString()));
        return event;
    }
}
