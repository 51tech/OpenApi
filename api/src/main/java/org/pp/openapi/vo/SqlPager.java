package org.pp.openapi.vo;

import lombok.Data;

/**
 * 分页的排序模型
 */
@Data
public class SqlPager {
    Integer limit =10;
    Integer page = 1;
    Integer offset = null;
    String order = null;

    public SqlPager(){}

    public SqlPager(Integer limit, Integer page){
        this.limit = limit;
        this.page = page;
    }
}
