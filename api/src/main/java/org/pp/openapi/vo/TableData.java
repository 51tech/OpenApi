package org.pp.openapi.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 表数据
 */
@Data
public class TableData{
    /**
     * 表代码
     */
    protected String table;

    /**
     * 模式
     */
    protected String schema;

    /**
     * 忽略事件
     */
    private Boolean ignore;

    /**
     * 数据
     */
    protected Map<String,Object> data = new HashMap<>();

    public void addData(String k, Object v){
        data.put(k, v);
    }

    public Object getData(String k){
        return data.get(k);
    }
}
