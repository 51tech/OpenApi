package org.pp.openapi.consts;

/**
 * 查询表达式
 */
public class ExpType {

    //等于 = 默认
    public static final String EQ = "eq";

    //不等于 !=
    public static final String NEQ = "neq";

    //模糊匹配 like '%v%'
    public static final String LIKE = "like";
    public static final String LK = "lk";

    //左匹配  like 'v%'
    public static final String LL = "ll";

    //右匹配  like '%v'
    public static final String RL = "rl";


    //小于 <
    public static final String LT = "lt";
    //小于等于 <=
    public static final String LTE = "lte";

    //大于 >
    public static final String GT = "gt";
    //大于等于 >=
    public static final String GTE = "gte";

    //为空
    public static final String NULL = "null";
    //不为空
    public static final String NOT_NULL = "not";

    //指定值之内 in (1 ,2 ,3)
    public static final String IN = "in";
    //不在指定的值之内
    public static final String NOT_IN = "nin";

    //两个数值之间 between v1 and v2
    public static final String BT = "bt";

    //子查询，需要预先配置在sql表
    public static final String SQL = "sql";
}
