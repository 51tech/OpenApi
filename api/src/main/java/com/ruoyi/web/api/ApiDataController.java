package com.ruoyi.web.api;

import org.pp.openapi.common.ApiResult;
import org.pp.openapi.common.OpenDataController;
import org.pp.openapi.service.IApiLoaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 接口控制器Controller
 *
 * @author yepanpan
 * @date 2023-07-10
 */
@RestController
@RequestMapping("/openapi")
public class ApiDataController extends OpenDataController {
    @Autowired
    private IApiLoaderService apiLoaderService;

    @GetMapping("/load/{type}")
    public ApiResult load(@PathVariable("type") String type){
        if("model".equalsIgnoreCase(type)){
            apiLoaderService.loadModel();
        }else if("event".equalsIgnoreCase(type)){
            apiLoaderService.loadEvent();
        }else if("sql".equalsIgnoreCase(type)){
            apiLoaderService.loadSql();
        }else{
            return ApiResult.error("不支持的操作");
        }
        return ApiResult.success();
    }
}
