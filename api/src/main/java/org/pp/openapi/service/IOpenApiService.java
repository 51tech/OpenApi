package org.pp.openapi.service;

import org.pp.openapi.common.ApiUser;

/**
 * 接口服务层
 */
public interface IOpenApiService {

    /**
     * 获取当前登录用户信息
     * @return
     */
    public ApiUser getLoginUser();

    /**
     * 判断当前登录用户是否超级管理员，可以管理模型
     * @return
     */
    public Boolean isAdmin();


    /**
     * 查询指定组织的下级组织
     * @param deptId 当前组织
     * @return
     */
    public String[] getSubDeptIds(String deptId);
}
