package org.pp.openapi.consts;

/**
 * 缓存主键
 */
public class CacheKeys {

    /** 数据模型 **/
    public static String CACHE_MODEL = "api_model:";

    /** 模型事件 **/
    public static String CACHE_EVENT = "api_event:";

    /** 内置脚本 **/
    public static String CACHE_SQL = "api_sql:";

    /** 访问控制 **/
    public static String CACHE_ACCESS = "api_access:";
}
