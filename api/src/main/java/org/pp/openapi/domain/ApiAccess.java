package org.pp.openapi.domain;

import lombok.Data;

import java.util.Map;

/**
 * 访问控制
 */
@Data
public class ApiAccess {
    /** 主键ID **/
    private Long id;

    /** 角色 **/
    private String roleId;

    /** 模型 **/
    private Long modelId;

    /** 可读 **/
    private Boolean read = false;

    /** 可写 **/
    private Boolean write = false;

    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiAccess fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("role_id")|| !data.containsKey("model_id")){
            return null;
        }
        ApiAccess access = new ApiAccess();
        access.setId(Long.parseLong(data.get("id").toString()));
        access.setRoleId(data.get("role_id").toString());
        access.setModelId(Long.parseLong(data.get("model_id").toString()));

        if("Y".equalsIgnoreCase(data.getOrDefault("read", "N").toString())){
            access.setRead(true);
        }
        if("Y".equalsIgnoreCase(data.getOrDefault("write", "N").toString())){
            access.setWrite(true);
        }
        return access;
    }
}
