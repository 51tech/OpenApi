package org.pp.openapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ruoyi.common.utils.StringUtils;
import lombok.Data;
import org.pp.openapi.utils.StrUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 模型
 */
@Data
public class ApiModel {
    /** 主键ID **/
    private Long id;

    /** 分组 **/
    private String group;

    /** 模型代码 **/
    private String code;

    /** 模型名称 **/
    private String name;

    /** 主键代码 **/
    private String pkField;

    /** 唯一键代码 **/
    private String[] uniqueField;

    /** 组织权限规则 **/
    private String orgScope;

    /** 用户权限规则 **/
    private String userScope;

    /** 详细说明 **/
    private String remark;

    /** 导入导出配置 **/
    private String excels;

    /** 是否内置模型 **/
    private Boolean isDefault;

    /** 状态 **/
    private String status;

    @JsonIgnore
    private List<ApiField> fields =new ArrayList<>();
    @JsonIgnore
    private Map<String, ApiAccess> accessMap = new HashMap<>();

    /**
     * 添加一个字段
     * @param field
     */
    public void addField(ApiField field){
        fields.add(field);
    }

    /**
     * 添加一个权限
     * @param access
     */
    public void addAccess(ApiAccess access){
        accessMap.put(access.getRoleId(), access);
    }

    /**
     * 类型转换
     * @param data
     * @return
     */
    public static ApiModel fromMap(Map data){
        if(!data.containsKey("id") || !data.containsKey("code")){
            return null;
        }
        ApiModel model = new ApiModel();
        model.setId(Long.parseLong(data.get("id").toString()));
        model.setCode(data.get("code").toString());
        if(data.containsKey("name")){
            model.setName(data.get("name").toString());
        }else{
            model.setName(model.getCode());
        }
        model.setPkField(data.getOrDefault("pk_field", "id").toString());
        String ufs = data.getOrDefault("unique_field", "").toString();
        if(StrUtil.isNotEmpty(ufs.trim())) {
            model.setUniqueField(ufs.split(","));
        }
        model.setGroup(data.getOrDefault("group", "默认").toString());
        model.setOrgScope(data.getOrDefault("org_scope", "").toString());
        model.setUserScope(data.getOrDefault("user_scope", "").toString());
        model.setExcels(data.getOrDefault("excels", "").toString());
        model.setRemark(data.getOrDefault("remark", "").toString());
        model.setIsDefault("Y".equalsIgnoreCase(data.getOrDefault("is_default", "N").toString()));
        model.setStatus(data.getOrDefault("status", "1").toString());
        return model;
    }
}
