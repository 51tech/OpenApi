package com.ruoyi.web.api;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysDeptService;
import org.pp.openapi.common.ApiUser;
import org.pp.openapi.service.IOpenApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpenApiServiceImpl implements IOpenApiService {

    @Autowired
    private ISysDeptService sysDeptService;

    @Override
    public ApiUser getLoginUser() {
        ApiUser user = new ApiUser();
        SysUser sysUser = SecurityUtils.getLoginUser().getUser();
        user.setUserId(sysUser.getUserId()+"");
        user.setAccount(sysUser.getUserName());
        user.setUsername(sysUser.getNickName());
        user.setDeptId(sysUser.getDeptId()+"");
        user.setRoles(sysUser.getRoles().stream().map(r->r.getRoleId().toString()).toArray(String[]::new));
        return user;
    }

    @Override
    public Boolean isAdmin() {
        return SecurityUtils.getUserId().equals(1L);
    }


    @Override
    public String[] getSubDeptIds(String deptId) {
        SysDept sysDept = sysDeptService.selectDeptById(Long.parseLong(deptId));
        String deptIds = (sysDept == null ? deptId : sysDept.getAncestors()+","+deptId);
        return deptIds.split(",");
    }
}
