package org.pp.openapi.service;

import org.pp.openapi.vo.TableExcel;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * 导入导出服务
 */
public interface IOpenExcelService {
    /**
     * 解析导入配置
     * @param importStr
     * @return
     */
    public TableExcel parseImport(String importStr);

    /**
     * 解析导出配置
     * @param exportStr
     * @return
     */
    public TableExcel parseExport(String exportStr);

    /**
     * 导出数据
     * @param paramStr 查询请求
     * @param os 输出流
     * @return
     */
    public int exports(String paramStr, OutputStream os);

    /**
     * 导入数据
     * @param inputStream 输入流
     * @param table 数据表
     * @param override 覆盖原有数据
     * @return
     */
    public String imports(InputStream inputStream, String table, boolean override);

    /**
     * 导入模板
     * @param table 数据表
     * @param os 输出流
     * @return
     */
    public String template(String table, OutputStream os);
}
