package org.pp.openapi.vo;

import lombok.Data;

/**
 * 字段模型
 */
@Data
public class SqlField {
    private String field;
    private String alias;
    private String ref;

    public SqlField(String field){
        String[] fs = field.split(":");
        this.field = fs[0];
        if(fs.length == 2){
            this.alias = fs[1];
        }
    }

    /**
     * 字段名
     * @return
     */
    public String getName(){
        String name = field;
        if(field.indexOf(')') == -1){
             name = "`"+field+"`";
        }
        if(alias != null){
            name += " " + alias;
        }
        return name;
    }
}
