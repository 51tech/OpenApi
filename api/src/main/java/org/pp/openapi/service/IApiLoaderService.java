package org.pp.openapi.service;

import org.pp.openapi.domain.ApiEvent;
import org.pp.openapi.domain.ApiModel;
import org.pp.openapi.domain.ApiSql;

/**
 * 自动加载接口相关资源
 */
public interface IApiLoaderService {

    /**
     * 查询缓存
     * @param cacheKey
     * @param <T>
     * @return
     */
    public <T> T getCacheObject(String cacheKey);

    /**
     * 设置缓存
     * @param cacheKey
     * @param cacheVal
     * @param <T>
     */
    public <T> void setCacheObject(String cacheKey, T cacheVal);

    /**
     * 清空指定前缀的缓存
     * @param cacheKeyPrefix
     */
    public void removeCaches(String cacheKeyPrefix);

    /**
     * 删除一个缓存
     * @param cacheKey
     */
    public void removeCache(String cacheKey);

    /**
     * 加载模型
     */
    public int loadModel();

    /**
     * 加载事件
     */
    public int loadEvent();

    /**
     * 加载内置SQL
     */
    public int loadSql();

    /**
     * 查询模型
     * @param table
     * @return
     */
    public ApiModel getModel(String table);

    /**
     * 查询模型事件
     * @param modelId
     * @param type
     * @return
     */
    public ApiEvent getEvent(Long modelId, String type);

    /**
     * 查询内置脚本
     * @param code
     * @return
     */
    public ApiSql getSql(String code);

    /**
     * 重置缓存
     * @param type 类型：model模型 event事件 sql内置脚本
     * @param ids 主键数组
     * @return
     */
    public int reload(String type, Long[] ids);
}
