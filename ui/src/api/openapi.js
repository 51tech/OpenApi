import request from '@/utils/request'


// POST提交数据
export function restPost(data) {
    return request({
        url: '/openapi',
        method: 'post',
        data: data
    })
}
export function apiPost(data) {
    return request({
        url: '/openapi/post',
        method: 'post',
        data: data
    })
}


// PUT提交数据
export function restPut(data) {
    return request({
        url: '/openapi',
        method: 'put',
        data: data
    })
}
export function apiPut(data) {
    return request({
        url: '/openapi/put',
        method: 'post',
        data: data
    })
}


// PATCH提交数据
export function restPatch(data) {
    return request({
        url: '/openapi',
        method: 'patch',
        data: data
    })
}
export function apiPatch(data) {
    return request({
        url: '/openapi/patch',
        method: 'post',
        data: data
    })
}


// GET请求
export function restGet(data) {
    return request({
        url: '/openapi/' + JSON.stringify(data),
        method: 'get'
    })
}
export function apiGet(data) {
    return request({
        url: '/openapi/get',
        data: data,
        method: 'post'
    })
}

// DELETE请求
export function restDelete(data) {
    return request({
        url: '/openapi/' + JSON.stringify(data),
        method: 'delete'
    })
}

export function apiDelete(data) {
    return request({
        url: '/openapi/delete',
        data: data,
        method: 'post'
    })
}

// 加载核心缓存
export function loadCache(type) {
    return request({
        url: '/openapi/load/'+type,
        method: 'get'
    })
}